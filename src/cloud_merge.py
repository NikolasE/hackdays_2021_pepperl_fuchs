#! /usr/bin/python3

import numpy as np
import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import LaserScan, PointField


class CloudMerger:
    def __init__(self):
        super().__init__()
        self.cloud = None

        self.__angle_min = 0.0
        self.__angle_max = 0.0
        self.__cos_sin_map = np.array([[]])
        self.point_collection = list()
        self.ones = None

    def laser2xy(self, scan_in: LaserScan, x_offset: float = 0):
        N = len(scan_in.ranges)
        if (
            self.__cos_sin_map.shape[1] != N
            or self.__angle_min != scan_in.angle_min
            or self.__angle_max != scan_in.angle_max
        ):
            # rospy.loginfo("No precomputed map given. Computing one.")

            self.__angle_min = scan_in.angle_min
            self.__angle_max = scan_in.angle_max

            angles = scan_in.angle_min + np.arange(N) * scan_in.angle_increment
            cos_ = np.cos(angles)
            sin_ = np.sin(angles)
            # self.ones = np.ones_like(cos_)
            self.__cos_sin_map = np.array([sin_, -cos_])

        np_arr = np.array(scan_in.ranges) * self.__cos_sin_map
        yz = np_arr.transpose().tolist()
        return [[x_offset, p[0], p[1]] for p in yz]

    def append_points(self, points):
        self.point_collection.extend(points)

    def clear_collection(self):
        self.point_collection.clear()

    def create_cloud(self, points, header, step: int = 1):
        fields = [
            PointField("x", 0, PointField.FLOAT32, 1),
            PointField("y", 4, PointField.FLOAT32, 1),
            PointField("z", 8, PointField.FLOAT32, 1),
        ]
        return pc2.create_cloud(header, fields, points[::step])
