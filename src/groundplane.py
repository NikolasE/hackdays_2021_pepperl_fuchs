#! /usr/bin/python3

from typing import Tuple

import geometry_msgs
import rospy
import tf
import tf2_ros

_DEFAULT_WORLD_GROUND_TF_TRANSLAT = (0, 0, 1)
_DEFAULT_WORLD_GROUND_TF_ROTATION = (0, 0, 0)


class Groundplane:
    def __init__(
        self,
        child_frame_id: str = "laser_base",
        translation_xyz: Tuple[float, ...] = _DEFAULT_WORLD_GROUND_TF_TRANSLAT,
        rotation_ypr: Tuple[float, ...] = _DEFAULT_WORLD_GROUND_TF_ROTATION,
    ) -> None:
        self._broadcaster = tf2_ros.StaticTransformBroadcaster()
        self._child_frame_id = child_frame_id
        self._translation = translation_xyz
        self._rotation = rotation_ypr

    def publish(self):
        static_transformStamped = geometry_msgs.msg.TransformStamped()
        static_transformStamped.header.stamp = rospy.Time.now()
        static_transformStamped.header.frame_id = "ground_plane"
        static_transformStamped.child_frame_id = self._child_frame_id

        static_transformStamped.transform.translation.x = self._translation[0]
        static_transformStamped.transform.translation.y = self._translation[1]
        static_transformStamped.transform.translation.z = self._translation[2]

        quat = tf.transformations.quaternion_from_euler(*self._rotation)
        static_transformStamped.transform.rotation.x = quat[0]
        static_transformStamped.transform.rotation.y = quat[1]
        static_transformStamped.transform.rotation.z = quat[2]
        static_transformStamped.transform.rotation.w = quat[3]

        self._broadcaster.sendTransform(static_transformStamped)
        rospy.sleep(1)


if __name__ == "__main__":
    rospy.init_node("ground_plane_publisher")
    Groundplane().publish()
    rospy.spin()
