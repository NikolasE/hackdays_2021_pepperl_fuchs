import math

import rospy
from tf.transformations import quaternion_from_euler
from visualization_msgs.msg import Marker


# https://grabcad.com/library/conveyor-belt
# Convert to dae format e.g. in Onshape.


_pub = rospy.Publisher("/conveyor", Marker, queue_size=1)
def publish_conveyor() -> None:
    msg = Marker()
    msg.type = Marker.MESH_RESOURCE
    msg.mesh_resource = "file:///home/laurenz/Downloads/Conveyor.dae"
    msg.mesh_use_embedded_materials = True
    msg.header.frame_id = "moving_laser"
    msg.frame_locked = True
    msg.color.a = 1
    msg.scale.x = 1
    msg.scale.y = 1
    msg.scale.z = 1
    q = quaternion_from_euler(math.pi / 2, -math.pi / 2, 0)
    msg.pose.orientation.x = q[0]
    msg.pose.orientation.y = q[1]
    msg.pose.orientation.z = q[2]
    msg.pose.orientation.w = q[3]
    msg.pose.position.x = 1.05
    msg.pose.position.z = -1.0
    _pub.publish(msg)


if __name__ == "__main__":
    rospy.init_node("publish_conveyor")
    publish_conveyor()
