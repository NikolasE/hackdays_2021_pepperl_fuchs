from dataclasses import dataclass
from pathlib import Path
from typing import Iterable, Optional, Sequence

import numpy as np
import pandas as pd
import rospy
from sklearn.neighbors import KNeighborsClassifier
from visualization_msgs.msg import Marker


MAX_OBSERVATION_KNN_DIST = 0.035

_DEFAULT_ALPHA = 0.85

UNKNOWN_LABEL = 'unknown'

# label str to [r,g,b,a]
LABEL_TO_COLOR_MAP = {
    '1': [0, 0, 1, _DEFAULT_ALPHA],
    '2': [0, 1, 0, _DEFAULT_ALPHA],
    '3': [0, 1, 1, _DEFAULT_ALPHA],
    '5': [1, 0, 0, _DEFAULT_ALPHA],
    'hollow': [.5, 0, .5, _DEFAULT_ALPHA],
    UNKNOWN_LABEL: [.5, 0, .5, _DEFAULT_ALPHA],
}


_DEFAULT_BOX_SIZES_PATHS = (Path(__file__).parent / '..' / 'data').glob('*box_sizes.txt')


@dataclass
class Dataset:
    xyz: np.array
    labels: np.array


def read_dataset(paths: Optional[Iterable[Path]] = None):
    paths = paths or _DEFAULT_BOX_SIZES_PATHS
    dfs = []
    for path in paths:
        spl = path.name.split('_')
        label = spl[1]
        df = pd.read_csv(path, names=['x', 'y', 'z', 'bag'], sep=" ")
        df['label'] = label
        # if spl[0] == 'jonas':
            # df.z += 0.02
        dfs.append(df)
    df = pd.concat(dfs, ignore_index=True)
    # sort xyz by size to become size independent
    return Dataset(np.sort(df.to_numpy()[:, :3]), df['label'])


class Classifier:
    def __init__(self, paths: Optional[Iterable[Path]] = None) -> None:
        # Check this for parameters:
        # https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html
        self._knn = KNeighborsClassifier()
        dataset = read_dataset(paths)
        self._knn.fit(dataset.xyz, dataset.labels)

    def classify(self, xyz: Sequence[float]) -> str:
        xyz = sorted(xyz)
        label = self._knn.predict([xyz])[0]
        dists, _ = self._knn.kneighbors([xyz], return_distance=True)
        if np.min(dists) > MAX_OBSERVATION_KNN_DIST:
            return UNKNOWN_LABEL
        return label



if __name__ == '__main__':
    print(Classifier().classify([.1, .2, .3]))
