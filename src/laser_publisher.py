from copy import deepcopy
from math import pi

import rospy
import tf2_ros
from geometry_msgs.msg import Quaternion, TransformStamped
from sensor_msgs.msg import LaserScan, PointCloud2
from tf2_msgs.msg import TFMessage
from tf.transformations import quaternion_from_euler
import numpy as np
import random

from src.cloud_merge import CloudMerger
from src.packet import ScanA

_CLOUD_DECIMATION = 1
_CLOUD_Z_CUTOFF_M = 0.06
_CLOUD_X_CUTOFF_M = 0.30

class LaserPublisher:

    WORLD_FRAME = "laser_base"

    offset = 0.0

    def __init__(self):
        self._scan_pub = rospy.Publisher("scan", LaserScan, queue_size=1)
        self._pub_tf = rospy.Publisher("tf", TFMessage, queue_size=1)
        self._pub_cloud = rospy.Publisher("cloud", PointCloud2, queue_size=1)

        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)

        self.ground_cloud = None
        self.moving_laser_frame = "moving_laser"

        self.cm = CloudMerger()

    def _create_tf(self, id_: int, offset: float, stamp):
        tfs = TransformStamped()
        tfs.header.frame_id = LaserPublisher.WORLD_FRAME
        tfs.header.stamp = stamp
        tfs.child_frame_id = "frame_%i" % id_

        q = quaternion_from_euler(0, pi / 2, 0)

        tfs.transform.rotation = Quaternion(x=q[0], y=q[1], z=q[2], w=q[3])
        tfs.transform.translation.x = offset
        return tfs

    def publish(self, scan: ScanA, moving_tf: bool = False, tstamp: float = -1):
        laser_msg = scan.to_laser_msg()

        if moving_tf:
            tfs = self._create_tf(scan.scan_number, self.offset, laser_msg.header.stamp)

            laser_msg.header.frame_id = tfs.child_frame_id

            tfm = TFMessage()
            laser_msg.header.frame_id = tfs.child_frame_id
            tfs.header.stamp = rospy.Time.from_sec(scan.timestamp.timestamp() - 0.2)

            tfs2 = deepcopy(tfs)
            tfs2.header.stamp = rospy.Time.from_sec(scan.timestamp.timestamp() + 0.2)

            tf_moving = deepcopy(tfs)
            tf_moving.header.stamp = rospy.Time.from_sec(scan.timestamp.timestamp() + 0.2)
            tf_moving.child_frame_id = self.moving_laser_frame

            self._pub_tf.publish(TFMessage(transforms=[tfs, tfs2, tf_moving]))
        else:
            laser_msg.header.frame_id = "laser_base"

        with_cloud = True
        if with_cloud:
            points = self.cm.laser2xy(laser_msg, x_offset=self.offset)

            # HACK: move to ground frame (same as applying self.m44)
            points = [[p[0], p[1], p[2] + 1] for p in points]

            # filter:
            max_abs_y = _CLOUD_X_CUTOFF_M
            min_z = _CLOUD_Z_CUTOFF_M
            points = [p for p in points if abs(p[1]) < max_abs_y and p[2] > min_z]

            if points:
                self.cm.append_points(points)

                if len(self.cm.point_collection) > 1000:
                    subsampled_cloud = list(random.sample(self.cm.point_collection, 1000))
                else:
                    subsampled_cloud = self.cm.point_collection

                self.ground_cloud = self.cm.create_cloud(subsampled_cloud, deepcopy(laser_msg.header))
                self.ground_cloud.header.frame_id = "ground_plane"

                # cloud_out = do_transform_cloud(cloud, self.laser_to_ground)
                self._pub_cloud.publish(self.ground_cloud)
            else:
                # remove points from collection after an empty scan was found (e.g. after an object)
                self.cm.point_collection.clear()
                # self.offset = 0

        self._scan_pub.publish(laser_msg)

        LaserPublisher.offset += 0.0085
