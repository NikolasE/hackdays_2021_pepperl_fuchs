import datetime
import struct

_NTP_OFFSET_S = 2208988800


def to_datetime(data: bytearray) -> datetime.datetime:
    secs_since_1900, fractional_sec = struct.unpack("II", data)
    secs_since_1970 = secs_since_1900 - _NTP_OFFSET_S
    fractional_sec /= 0xFFFFFFFF
    secs_since_1970 += fractional_sec
    ts = datetime.datetime.fromtimestamp(secs_since_1970, tz=datetime.timezone.utc)
    return ts
