#! /usr/bin/env python3

import logging
import os
from pathlib import Path

import rosbag
import rospy
from sensor_msgs.msg import PointCloud2
from visualization_msgs.msg import Marker

from src.groundplane import Groundplane
from src.projection import CloudToXYImage
from src.receive import DemoFileReceiver, PcapReceiver, SocketReceiver


INPUT_FILES = (Path().home() / 'Documents' / 'training_set_1').glob('*/*/*')


def batch_cloud_creation() -> None:
    for path in INPUT_FILES:
        if path.suffix != '':
            continue  # only handle input files. they have no extension
        bag_path = path.with_suffix('.bag').resolve()
        if bag_path.is_file():
            continue  # skip already processed files
        receiver = DemoFileReceiver(str(path.resolve()), slow=False, bag_path=str(bag_path))
        Groundplane().publish()
        receiver.run()


def batch_cloud_processing() -> None:
    pub = rospy.Publisher("/cloud", PointCloud2, queue_size=1)


    # ds = ("jonas", [1,2,3,5])
    ds = ("fabian", [1,2,3])
    
    training_dir = os.path.join(os.path.expanduser("~"), "Documents/training_set_1/")

    for i in ds[1]:
        rospy.logwarn(f"XXXXXXXXx   Processing {i}")
        directory = os.path.join(training_dir, "%s/%i" % (ds[0],i))
        processor = CloudToXYImage()


        # directory = os.path.join(os.path.expanduser("~"), "Documents/training_set_1/%s/%i" % (ds[0],i))

        # size_file_path os.path.join(os.path.expanduser("~"), "Documents/training_set_1/evaluated/%s_%i_box_sizes.txt" % (ds[0],i))
        size_file_path = os.path.join(training_dir, "evaluated", "%s_%i_box_sizes.txt" % (ds[0], i))

        with open(size_file_path, "w") as size_file:
            for f in os.listdir(directory):
                if not f.endswith(".bag"):
                    continue

                full_path = os.path.join(directory, f)
                try:
                    bag = rosbag.Bag(full_path)
                except Exception:
                    # print(full_path)
                    continue

                for topic, msg, t in bag.read_messages(topics=["/cloud"]):
                    pub.publish(msg)

                    try:
                        marker = rospy.wait_for_message("/rect_marker", Marker, rospy.Duration(10))
                    except Exception:
                        rospy.logwarn(f"Could not process {full_path}")
                        continue

                    width = min(marker.scale.x, marker.scale.y)
                    length = max(marker.scale.x, marker.scale.y)

                    msg = "%.3f %.3f %.3f %s\n" % (width, length, marker.scale.z, f)
                    size_file.write(msg)
                    print(msg.strip())
                    rospy.sleep(0.5) # don't remove me! otherwise data could mixed up!
        



if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    rospy.init_node("train")
    # batch_cloud_creation()
    batch_cloud_processing()
