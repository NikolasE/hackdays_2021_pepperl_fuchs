#! /usr/bin/env python3

from typing import Tuple
import logging
import os
import socket
import time

from scapy.layers.inet import UDP, defragment  # noqa: F401
from scapy.utils import rdpcap

from src.parser import FileParser

_DEFAULT_RECEIVE_PORT = 5555
_UDP_DGRAM_BUF_SIZE = 2048  # should be a multiple of 1024, and higher than the MTU


_DEMO_INPUT_FILE = os.path.join(os.path.expanduser("~"), "Documents/training_set_1/fabian/2/1614267426349")
# '/home/engelhard/Downloads/training_set_1/fabian/1/1614267741368'


class Receiver:
    def __init__(self) -> None:
        self._parser = FileParser()
        self._stop = False

    def _receive(self) -> Tuple[bytearray, float]:
        raise NotImplementedError()

    def run(self) -> None:
        while not self._stop:
            try:
                msg, tstamp = self._receive()
            except StopIteration:
                break
            try:
                packet = FileParser.parse_frame(msg)
            except RuntimeError as exc:
                print(exc)
            else:
                self._parser.aggregate_and_publish(packet, tstamp=tstamp)


class SocketReceiver(Receiver):
    def __init__(self, receive_port=_DEFAULT_RECEIVE_PORT) -> None:
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.bind(("", receive_port))
        super().__init__()

    def _receive(self) -> Tuple[bytearray, float]:
        msg, addr = self._socket.recvfrom(_UDP_DGRAM_BUF_SIZE)
        logging.debug("Received %s (%sB) from %s", repr(msg), len(msg), addr)
        return bytearray(msg), time.time()


class PcapReceiver(Receiver):
    def __init__(self, pcap_file: str, slow=True) -> None:
        self._packets = iter(defragment(rdpcap(pcap_file)))
        self._slow = slow
        super().__init__()

    def _receive(self) -> Tuple[bytearray, float]:
        while True:
            try:
                msg = next(self._packets)
                payload = msg["UDP"].payload
                tstamp = float(msg.time)
            except IndexError:
                continue  # not a UDP packet
            logging.debug("Read %s (%sB)", repr(payload), len(payload))
            if self._slow:
                time.sleep(1 / 150)
            return bytearray(bytes(payload)), tstamp


class DemoFileReceiver(Receiver):
    def __init__(self, path: str = _DEMO_INPUT_FILE, bag_path: str = "", slow=True) -> None:
        with open(path, "r") as f:
            d = f.read()
        self._spl = d.split(",")
        self._slow = slow
        self._pos = 0
        super().__init__()
        if bag_path:
            print(f"Bag will be written to {bag_path}")
            self._parser.set_final_bag_path(bag_path)

    def _receive(self) -> Tuple[bytearray, float]:
        while True:
            try:
                ndx = self._spl.index("|", self._pos)
            except ValueError as exc:
                raise StopIteration from exc
            msg = self._spl[self._pos : ndx]
            self._pos = ndx + 1
            ints = list(map(int, msg))
            if self._slow:
                time.sleep(1 / 150)
            return bytearray(ints), time.time()

