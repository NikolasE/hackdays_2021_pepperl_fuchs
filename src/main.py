import logging
from pathlib import Path
from random import shuffle

import rospy

from src.groundplane import Groundplane
from src.projection import CloudToXYImage
from src.receive import DemoFileReceiver, PcapReceiver, SocketReceiver
from src.conveyor import publish_conveyor


# TRAINING_SET = "training_set_1"
TRAINING_SET = "training_set_2"
# TRAINING_SET = "training_set_3_open_boxes"
DEMO_PATHS = [path for path in (Path.home() / "Documents" / TRAINING_SET).glob("*/*/*") if path.suffix == '']
shuffle(DEMO_PATHS)

PORT_JONAS = 5555
PORT_FABIAN = 6666

PCAP_FILES = Path("recordings").glob("*.pcap")


def main() -> None:
    """Main."""
    rospy.init_node("laser_scan_publisher")
    Groundplane().publish()

    detector = CloudToXYImage()

    publish_conveyor()

    # receiver = SocketReceiver(PORT_JONAS)
    # receiver = SocketReceiver(PORT_FABIAN)
    # receiver.run()

    for path in PCAP_FILES:
        print(path)
        receiver = PcapReceiver(str(path), slow=False)
        receiver.run()

    # for path in DEMO_PATHS:
    #     receiver = DemoFileReceiver(str(path), slow=True)
    #     receiver.run()


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    main()
