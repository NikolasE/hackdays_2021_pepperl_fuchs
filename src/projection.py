#! /usr/bin/env python3

# The "right" way:
# https://pcl.readthedocs.io/projects/tutorials/en/latest/project_inliers.html

from dataclasses import dataclass
from math import pi
from typing import Iterator, Tuple

import cv2
import numpy as np
import rospy
import sensor_msgs.point_cloud2 as pc2
from cv_bridge import CvBridge
from geometry_msgs.msg import Quaternion
from sensor_msgs.msg import Image, PointCloud2
from tf.transformations import quaternion_from_euler
from visualization_msgs.msg import Marker

from src.classify import Classifier, LABEL_TO_COLOR_MAP


_DEFAULT_IMG_PIXEL_PER_M = 1000.0
_DEFAULT_MAX_Z_M = 0.30
_DEFAULT_MIN_Z_M = 0
_POINT_FILLOUT_RADIUS_M = 0.0075

_BORDER_M = 0.05
_ANNOTATION_LINEWIDTH = 5

_DETECT_HOLLOW_BOXES = True

@dataclass
class ImageExtents:
    min_x: float
    max_x: float
    min_y: float
    max_y: float
    min_z: float
    max_z: float


def _get_xyz_generator(cloud: PointCloud2) -> Iterator[Tuple[float, float, float]]:
    return pc2.read_points(cloud, skip_nans=True, field_names=("x", "y", "z"))


def _to_pixel_value(z: float) -> int:
    return int(np.interp(z, [_DEFAULT_MIN_Z_M, _DEFAULT_MAX_Z_M], [0, 255]))


class CloudToXYImage:
    def __init__(self, image_resolution_m: float = _DEFAULT_IMG_PIXEL_PER_M) -> None:
        self._subscriber = rospy.Subscriber("/cloud", PointCloud2, self._cloud_callback, queue_size=1)
        self._px_per_m = image_resolution_m
        self._bridge = CvBridge()
        self._pub_marker = rospy.Publisher("colored_marker", Marker, queue_size=1)
        self._pub_debugimg = rospy.Publisher("debug_img", Image, queue_size=1)
        self._text_pub = rospy.Publisher("/classification_text", Marker, queue_size=1)
        self._classifier = Classifier()

    def _to_center_pixel(self, xyz: Tuple[float, ...], extents: ImageExtents) -> Tuple[int, int]:
        return (
            int(np.round(self._px_per_m * (_BORDER_M + xyz[0] - extents.min_x))),
            int(np.round(self._px_per_m * (_BORDER_M + xyz[1] - extents.min_y))),
        )

    def _cloud_callback(self, cloud: PointCloud2):
        points = list(_get_xyz_generator(cloud))
        extents = ImageExtents(
            min_x=min(xyz[0] for xyz in points),
            max_x=max(xyz[0] for xyz in points),
            min_y=min(xyz[1] for xyz in points),
            max_y=max(xyz[1] for xyz in points),
            min_z=min(xyz[2] for xyz in points),
            max_z=max(xyz[2] for xyz in points),
        )
        img_size_x = int(np.ceil(self._px_per_m * (2 * _BORDER_M + extents.max_x - extents.min_x))) + 1
        img_size_y = int(np.ceil(self._px_per_m * (2 * _BORDER_M + extents.max_y - extents.min_y))) + 1

        image = np.zeros((img_size_x, img_size_y), np.float32)
        for xyz in points:
            x, y = self._to_center_pixel(xyz, extents)
            radius = int(np.round(_POINT_FILLOUT_RADIUS_M * _DEFAULT_IMG_PIXEL_PER_M))
            image = cv2.circle(image, (y, x), radius, _to_pixel_value(xyz[2]), -1)

        xy = np.array([[[xyz[0], xyz[1]] for xyz in points]], dtype=np.float32)
        rect = cv2.minAreaRect(xy)
        
        mean_z = float(np.mean([xyz[2] for xyz in points]))
        box_size = [rect[1][0], rect[1][1], mean_z]

        max_lateral_box_dim = max(box_size[0], box_size[1])
        if max_lateral_box_dim < 0.04:
            return  # nothing to see here, move along. no detection for you.

        label = None
        if _DETECT_HOLLOW_BOXES:
            min_box_dim = min(box_size[0], box_size[1])
            no_points_under_this_dist_from_center_allowed = min_box_dim * 0.1
            for x, y, _ in points:
                if np.hypot(x - rect[0][0], y - rect[0][1]) < no_points_under_this_dist_from_center_allowed:
                    break
            else:
                label = 'hollow'

        # Classify!
        if label is None:
            label = self._classifier.classify([box_size[0], box_size[1], box_size[2]])

        # Publish image
        box = np.array([self._to_center_pixel(xypoint, extents)[::-1] for xypoint in cv2.boxPoints(rect)], dtype=np.int)
        col_img = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        annot_color = [255 * c for c in LABEL_TO_COLOR_MAP[label][:3][::-1]]
        col_img = cv2.drawContours(col_img, [box], 0, annot_color, _ANNOTATION_LINEWIDTH)
        image_message = self._bridge.cv2_to_imgmsg(col_img.astype(np.uint8), "passthrough")
        self._pub_debugimg.publish(image_message)

        # Publish markers
        height = float(np.max([xyz[2] for xyz in points]))
        self.publish_as_markers(rect, header=cloud.header, height=height, label=label)

        return box_size

    def publish_as_markers(self, rect, header, height: float, label: str):
        marker = Marker()
        marker.header = header
        # marker.id = np.int32(hash(rospy.Time.now().to_sec()))
        # marker.lifetime = rospy.Duration(3)
        marker.type = marker.CUBE
        marker.scale.x = rect[1][0]
        marker.scale.y = rect[1][1]
        marker.scale.z = height

        q = quaternion_from_euler(0, 0, rect[2] / 180 * pi)
        marker.pose.orientation = Quaternion(x=q[0], y=q[1], z=q[2], w=q[3])
        marker.pose.position.x = rect[0][0]
        marker.pose.position.y = rect[0][1]
        marker.pose.position.z = height / 2

        color = LABEL_TO_COLOR_MAP[label]
        marker.color.r = color[0]
        marker.color.g = color[1]
        marker.color.b = color[2]
        marker.color.a = color[3]

        self._pub_marker.publish(marker)

        # Publish Text
        marker.type = Marker.TEXT_VIEW_FACING
        marker.text = label
        marker.color.r = 1
        marker.color.b = 1
        marker.color.g = 1
        marker.color.a = 1
        marker.pose.position.z += marker.scale.z / 2 + 0.10
        marker.scale.x = 1
        marker.scale.y = 1
        marker.scale.z = .2
        self._text_pub.publish(marker)


if __name__ == "__main__":
    rospy.init_node("cloud_to_xyimage")
    CloudToXYImage()
    rospy.spin()
