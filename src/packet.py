import datetime
from typing import List

import rospy
from sensor_msgs.msg import LaserScan


class ScanA:
    scan_number: int  # sequence number for scan (counting transmitted scans, starting with 0, overflows)
    pts_scan: int
    timestamp: datetime.datetime  # raw timestamp of first scan point in this packet
    scan_frequency_hz: float  # currently configured scan frequency
    first_angle_rad: float  # absolute angle of first scan point in this packet
    angular_increment_rad: float  # delta angle between two scan points (CCW >0, CW <0)
    distances: List[float]  # measured distance

    packet_number: int
    pts_packet: int

    def to_laser_msg(self):
        num_readings = len(self.distances)
        msg = LaserScan()
        msg.header.stamp = rospy.Time.from_sec(self.timestamp.timestamp())
        # msg.header.frame_id = frame_id

        msg.angle_min = self.first_angle_rad
        msg.angle_max = self.first_angle_rad + self.angular_increment_rad * num_readings
        msg.angle_increment = self.angular_increment_rad
        msg.time_increment = (1 / self.scan_frequency_hz) / num_readings
        msg.scan_time = 1 / self.scan_frequency_hz
        msg.range_min = 0.0
        msg.range_max = 2.0
        msg.ranges = self.distances
        msg.intensities = []
        return msg
