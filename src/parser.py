#! /usr/bin/python3

import struct
from datetime import datetime
from math import pi
import logging

import rosbag
import rospy

from src.laser_publisher import LaserPublisher
from src.packet import ScanA


logging.basicConfig(level=logging.DEBUG)

class FileParser:

    MAGIC_NUMBER = 41564  # 0xa25c

    def __init__(self):
        super().__init__()
        self.laser_pub = LaserPublisher()
        self._current_scan_id = None
        self._current_scans = []
        self.bag_path = None

    def set_final_bag_path(self, path: str):
        self.bag_path = path

    # uint16: H
    # uint32: I
    # unsigned long long: Q

    @staticmethod
    def parse_frame(ints) -> ScanA:
        magic = struct.unpack("H", bytearray(ints[:2]))[0]
        if magic != FileParser.MAGIC_NUMBER:
            raise RuntimeError("NO MAGIC!")

        parse_s = "HHIH"
        magic, packet_type, packet_size, header_size = struct.unpack(parse_s, bytearray(ints[:10]))
        # packet_type = chr(header[1])
        # print("Packet_type: %s" % chr(packet_type))
        # packet_size = header[2]

        assert packet_type == 65

        packet = ScanA()

        # if not packet_size == len(ints):
        #     raise RuntimeError("Wrong packet_size: got %i, but header expects %i" % (len(ints), packet_size))

        # header_size = header[3]
        # print("Header size: %i" % header_size)

        packet.scan_number, packet.packet_number = struct.unpack("HH", bytearray(ints[10:14]))
        # ts_raw, _ = struct.unpack("QQ", bytearray(ints[14:30]))

        packet.timestamp = datetime.now()

        # packet.timestamp = to_datetime(bytearray(ints[14:22]))

        # print((ints[14:30]))
        flags, frequency_1k = struct.unpack("II", bytearray(ints[30:38]))

        packet.scan_frequency_hz = frequency_1k / 1000

        packet.pts_scan, packet.pts_packet, packet.first_index = struct.unpack("HHH", bytearray(ints[38:44]))

        first_angle_10k, ang_ink_10k = struct.unpack("ii", bytearray(ints[44:52]))

        packet.first_angle_rad = first_angle_10k / 10000 / 180 * pi
        packet.angular_increment_rad = ang_ink_10k / 10000 / 180 * pi

        ints_available = len(bytearray(ints[header_size:])) / 4
        ints_available = int(min(ints_available, packet.pts_packet))
        distances = struct.unpack("I" * ints_available, bytearray(ints[header_size:]))
        missing_pts = packet.pts_packet - ints_available
        if missing_pts > 0:
            distances = list(distances)
            distances.extend([0xFFFFFFFF]*missing_pts)
        packet.distances = [d / 1000.0 for d in distances]
        # print("got package %i from scan %i with %i points (out of %i)" % (packet.packet_number, packet.scan_number, len(distances), packet.pts_scan))
        return packet

    @staticmethod
    def merge_scans(scans):
        if not scans:
            return
        if len(scans) != 3:
            raise RuntimeError(f"Got {len(scans)} packets for one scan, not supported")
        for i in range(3):
            if scans[i].packet_number != i + 1:
                raise RuntimeError(f"Got packet number {scans[i].packet_number}, but expected {i+1}.")

        for i in range(1, 3):
            scans[0].distances += scans[i].distances
        assert len(scans[0].distances) == scans[0].pts_scan

        scans[0].pts_packet = 0

        return scans[0]

    def aggregate_and_publish(self, packet: ScanA, sleep: bool = False, tstamp: float = -1) -> None:
        if packet.scan_number != self._current_scan_id:
            try:
                full_scan = FileParser.merge_scans(self._current_scans)
            except RuntimeError as exc:
                logging.exception("Could not merge scans")
                print(f"Could not merge scans: {exc}")
                self._current_scan_id = packet.scan_number
                self._current_scans = [packet]
                return
            if full_scan:
                self.laser_pub.publish(full_scan, moving_tf=True, tstamp=tstamp)
                if sleep:
                    rospy.sleep(1 / full_scan.scan_frequency_hz / 5)
            self._current_scan_id = packet.scan_number
            self._current_scans = [packet]
        else:
            self._current_scans.append(packet)

    def __del__(self):
        if self.bag_path:
            print(f"Writing cloud to {self.bag_path}")
            bag = rosbag.Bag(self.bag_path, "w")
            bag.write("/cloud", self.laser_pub.ground_cloud)
            bag.close()
